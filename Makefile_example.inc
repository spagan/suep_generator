# PYTHIA configuration file.
# Generated on Wed Oct 30 11:02:17 PDT 2019 with the user supplied options:
# --with-hepmc2=/share/apps/HepMC2/x86_64-slc5-gcc41-opt

# Install directory prefixes.
PREFIX_BIN=/home/smknapen/pythia8243_SUEP/bin
PREFIX_INCLUDE=/home/smknapen/pythia8243_SUEP/include
PREFIX_LIB=/home/smknapen/pythia8243_SUEP/lib
PREFIX_SHARE=/home/smknapen/pythia8243_SUEP/share/Pythia8

# Compilation flags (see ./configure --help for further documentation).
ENABLE_SHARED=false
CXX=/share/apps/gcc-4.8.3/bin/g++
CXX_COMMON=-O2  -pedantic -W -Wall -Wshadow -fPIC
CXX_SHARED=-shared
CXX_SONAME=-Wl,-soname,
LIB_SUFFIX=.so

# EVTGEN configuration.
EVTGEN_USE=false
EVTGEN_BIN=
EVTGEN_INCLUDE=
EVTGEN_LIB=

# FASTJET3 configuration.
FASTJET3_USE=false
FASTJET3_BIN=
FASTJET3_INCLUDE=
FASTJET3_LIB=

# HEPMC2 configuration.
HEPMC2_USE=true
HEPMC2_BIN=/share/apps/HepMC2/x86_64-slc5-gcc41-opt/bin
HEPMC2_INCLUDE=/share/apps/HepMC2/x86_64-slc5-gcc41-opt/include
HEPMC2_LIB=/share/apps/HepMC2/x86_64-slc5-gcc41-opt/lib

# HEPMC3 configuration.
HEPMC3_USE=false
HEPMC3_BIN=
HEPMC3_INCLUDE=
HEPMC3_LIB=

# LHAPDF5 configuration.
LHAPDF5_USE=false
LHAPDF5_BIN=
LHAPDF5_INCLUDE=
LHAPDF5_LIB=

# LHAPDF6 configuration.
LHAPDF6_USE=false
LHAPDF6_BIN=
LHAPDF6_INCLUDE=
LHAPDF6_LIB=

# POWHEG configuration.
POWHEG_USE=false
POWHEG_BIN=
POWHEG_INCLUDE=
POWHEG_LIB=

# PROMC configuration.
PROMC_USE=false
PROMC_BIN=
PROMC_INCLUDE=
PROMC_LIB=

# ROOT configuration.
ROOT_USE=false
ROOT_BIN=
ROOT_LIBS=
CXX_ROOT=

# GZIP configuration.
GZIP_USE=false
GZIP_BIN=
GZIP_INCLUDE=
GZIP_LIB=

# BOOST configuration.
BOOST_USE=false
BOOST_BIN=
BOOST_INCLUDE=
BOOST_LIB=

# PYTHON configuration.
PYTHON_USE=false
PYTHON_BIN=
PYTHON_INCLUDE=
PYTHON_LIB=
